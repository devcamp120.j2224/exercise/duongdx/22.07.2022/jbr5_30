package com.devcamp.s10.jbr5_30.Service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.s10.jbr5_30.model.Author;

@Service
public class AuthorService {
    Author  NgoTatTo = new Author("Ngo Tat To", "ngotatto@gmail.com", 'm');
    Author  VuTrongPhung = new Author("VuTrongPhung", "VuTrongPhung@gmail.com", 'm');
    Author  NamCao = new Author("NamCao", "NamCao@gmail.com", 'm');
    Author  NguyenDu = new Author("NguyenDu", "NguyenDu@gmail.com", 'm');
    Author  HoXuanHuong = new Author("HoXuanHuong", "HoXuanHuong@gmail.com", 'f');
    Author  XuanDieu = new Author("XuanDieu", "XuanDieu@gmail.com", 'm');

    public ArrayList<Author> getAuthor1() {
        ArrayList<Author> authorList = new ArrayList<>();
        authorList.add(NgoTatTo);
        authorList.add(VuTrongPhung);

        return authorList ;
    }

    public ArrayList<Author> getAuthor2() {
        ArrayList<Author> authorList = new ArrayList<>();
        authorList.add(NamCao);
        authorList.add(NguyenDu);

        return authorList ;
    }

    public ArrayList<Author> getAuthor3() {
        ArrayList<Author> authorList = new ArrayList<>();
        authorList.add(HoXuanHuong);
        authorList.add(XuanDieu);

        return authorList ;
    }
    public ArrayList<Author> getAuthorALl() {
        ArrayList<Author> authorList = new ArrayList<>();
        authorList.add(HoXuanHuong);
        authorList.add(XuanDieu);
        authorList.add(NamCao);
        authorList.add(NguyenDu);
        authorList.add(NgoTatTo);
        authorList.add(VuTrongPhung);
        
        return authorList ;
    }

}
