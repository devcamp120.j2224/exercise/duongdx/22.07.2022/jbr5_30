package com.devcamp.s10.jbr5_30.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.s10.jbr5_30.model.Book;

@Service
public class BookService {
    @Autowired
    private AuthorService authorServices;

    Book ThoCa = new Book("ThoCa", 12000, 2);
    Book DuongDai = new Book("DuongDai", 21000, 5);
    Book KiSu = new Book("KiSu", 43000, 12);

    public ArrayList<Book> getBooks() {
        ArrayList<Book> books = new ArrayList<Book>();
        ThoCa.setAuthor(authorServices.getAuthor1());
        DuongDai.setAuthor(authorServices.getAuthor2());
        KiSu.setAuthor(authorServices.getAuthor3());

        books.add(ThoCa);
        books.add(DuongDai);
        books.add(KiSu);
        return books ;
    }
}
