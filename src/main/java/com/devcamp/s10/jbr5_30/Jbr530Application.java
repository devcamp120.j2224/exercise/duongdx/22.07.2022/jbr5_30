package com.devcamp.s10.jbr5_30;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr530Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr530Application.class, args);
	}

}
