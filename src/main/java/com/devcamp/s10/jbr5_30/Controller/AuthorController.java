package com.devcamp.s10.jbr5_30.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.devcamp.s10.jbr5_30.Service.AuthorService;
import com.devcamp.s10.jbr5_30.model.Author;


@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class AuthorController {
    @Autowired
    private AuthorService authorService;

    @GetMapping("/author-info")
    public Author getAuthorInfo(@RequestParam("email") String email) {
        ArrayList<Author> authorList = authorService.getAuthorALl();
        Author authorfindgmail = new Author();
        for(Author author : authorList) {
            if(author.getEmail().equals(email)) {
                authorfindgmail = author ;
            }
        }
        return authorfindgmail;
    }


    @GetMapping("/author-gender")
    public  ArrayList<Author> getAuthorInfo(@RequestParam("gender") char gender) {
        ArrayList<Author> authorList = authorService.getAuthorALl();
        ArrayList<Author> authorfindgender = new ArrayList<>();
        for(Author author : authorList) {
            if(author.getGender() == gender) {
                authorfindgender.add(author) ;
            }
        }
        return authorfindgender;
    }
}
