package com.devcamp.s10.jbr5_30.Controller;

import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.devcamp.s10.jbr5_30.Service.BookService;
import com.devcamp.s10.jbr5_30.model.Book;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class BookController {
    @Autowired
    private BookService  bookService;

    @GetMapping("/books")
    public ArrayList<Book> getBooks() {
        ArrayList<Book> books = bookService.getBooks();
        return books;
    }

    @GetMapping("/book-quantity")
    public ArrayList<Book> getQuantityBooks(@RequestParam(value = "book-quantity", required = true) Integer intBook){
        ArrayList<Book> books = bookService.getBooks();
        ArrayList<Book> bookfind = new ArrayList<>();
        for(Book book : books){
            if(book.getQty() >= intBook){
                bookfind.add(book);
            }
        }
        return bookfind;
    }
}
