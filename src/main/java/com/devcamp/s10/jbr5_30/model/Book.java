package com.devcamp.s10.jbr5_30.model;

import java.util.ArrayList;

public class Book {
    private String name ;
    private ArrayList<Author> author ;
    private double price ;
    private int qty = 1;
    public Book() {
    }

    public Book(String name, ArrayList<Author> author, double price, int qty) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.qty = qty;
    }




    public Book(String name, double price, int qty) {
        this.name = name;
        this.price = price;
        this.qty = qty;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Author> getAuthor() {
        return author;
    }

    public void setAuthor(ArrayList<Author> author) {
        this.author = author;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    @Override
    public String toString() {
        return "Book [author=" + author + ", name=" + name + ", price=" + price + ", qty=" + qty + "]";
    }

    
}
